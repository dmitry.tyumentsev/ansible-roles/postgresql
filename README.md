PostgreSQL role
=========

Installs PostgreSQL on Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.postgresql
  vars:
    postgresql_users:
      - name: user
        password: password
    postgresql_databases:
      - name: database
        owner: user
    postgresql_hba_extra_entries:
      - type: host
        database: all
        user: all
        address: "0.0.0.0/0"
        method: scram-sha-256
```
